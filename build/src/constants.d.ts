export declare enum ChatEvent {
    CONNECT = "connect",
    DISCONNECT = "disconnect",
    MESSAGE = "message"
}
export declare enum TimerEvent {
    CONNECT = "connect",
    DISCONNECT = "disconnect",
    MESSAGE = "subscribeToTimer"
}
