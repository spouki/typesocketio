import express from 'express';
export declare class ChatServer {
    static readonly PORT: number;
    private _app;
    private server;
    private io;
    private port;
    private users;
    constructor();
    private initRoutes;
    private initSocket;
    private listen;
    get app(): express.Application;
}
