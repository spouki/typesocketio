"use strict";
// src/constants.ts
Object.defineProperty(exports, "__esModule", { value: true });
var ChatEvent;
(function (ChatEvent) {
    ChatEvent["CONNECT"] = "connect";
    ChatEvent["DISCONNECT"] = "disconnect";
    ChatEvent["MESSAGE"] = "message";
})(ChatEvent = exports.ChatEvent || (exports.ChatEvent = {}));
var TimerEvent;
(function (TimerEvent) {
    TimerEvent["CONNECT"] = "connect";
    TimerEvent["DISCONNECT"] = "disconnect";
    TimerEvent["MESSAGE"] = "subscribeToTimer";
})(TimerEvent = exports.TimerEvent || (exports.TimerEvent = {}));
//
//# sourceMappingURL=constants.js.map