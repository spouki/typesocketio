// src/types.ts

export interface ChatMessage {
  author:string;
  message:string;
  date:string;
}

export interface ChatState {
  input:string;
  messages:ChatMessage[];
}

export interface User {
  id:string;
  username:string;
  password:string;
  status:string;
}

export interface TimerMessage {
  interval:number;
}

//
