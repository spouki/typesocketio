// ChatServer class properties
import express from 'express';
import socketIo from 'socket.io';
import { ChatEvent,TimerEvent } from './constants';
import { ChatMessage, TimerMessage } from './types';
import { User } from './types';
import { createServer, Server } from 'http';
// var db = require('nano')('http://localhost:5984/typesocketio');
var cors = require('cors');

export class ChatServer {
  public static readonly PORT: number = 8080;
  private _app: express.Application;
  private server: Server;
  private io!: SocketIO.Server;
  private port: string | number;
  private users:User[];

  constructor () {
    this._app = express();
    this.port = process.env.PORT || ChatServer.PORT;
    this._app.use(cors());
    this._app.options('*', cors());
    this.server = createServer(this._app);
    this.users = [];
    this.initRoutes();
    this.initSocket();
    this.listen();
  }

  private initRoutes(): void {
    this._app.get('/',(req:any,res:any)=>{
      console.log("Got a query");
    });
    this._app.post('/register', (req:any, res:any) => {
      console.log("Got a registration");
      res.send({"message":"ok"});
    });
  }

  private initSocket (): void {
    this.io = socketIo(this.server);
  }

  private listen (): void {
    this.server.listen(this.port, () => {
      console.log('Running server on port %s', this.port);
    });

    this.io.on(ChatEvent.CONNECT, (socket: any) => {
      console.log('Connected client on port %s.', this.port);

      socket.on(ChatEvent.MESSAGE, (m: ChatMessage) => {
        console.log('[server](message): %s', JSON.stringify(m));
        this.io.emit('message', m);
      });

      socket.on(TimerEvent.MESSAGE, (m: TimerMessage) => {
        console.log('[server](message): %s', JSON.stringify(m));
        setInterval(()=>{
          this.io.emit('timer',new Date());
        }, m.interval);
        // this.io.emit('message', m);
      });

      socket.on(ChatEvent.DISCONNECT, () => {
        console.log('Client disconnected');
      });
    });


  }

  get app (): express.Application {
    return this._app;
  }
}
//
