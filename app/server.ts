// import { app } from ".";
// const port = 3000;
//
// app.listen(port, () => {
//   `Now listening on ${port}`;
// });

import { ChatServer } from './src/ChatServer';

let app = new ChatServer().app;

export { app };
